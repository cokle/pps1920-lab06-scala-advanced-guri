package u06lab.solution

import u06lab.solution.TicTacToe.Player

import scala.jdk.CollectionConverters._

object TicTacToe extends App {
  sealed trait Player{
    def other: Player = this match {case X => O; case _ => X}
    override def toString: String = this match {case X => "X"; case _ => "O"}
  }
  case object X extends Player
  case object O extends Player

  case class Mark(x: Int, y: Int, player: Player)
  type Board = List[Mark]
  type Game = List[Board]
  type Position = (Int, Int)

  def find(board: Board, x: Int, y: Int): Option[Player] =
    board.collect( {case m if(m.x == x && m.y == y) => m.player;}) match {
      case h::t =>  Option(h);
      case _ => None
    }

  def placeAnyMark(board: Board, player: Player): Seq[Board] = {
    var boards:List[Board] = List()
    for (x <- 0 to 2; y <- 0 to 2; if(find(board, x, y).isEmpty)) {
      boards ++=  List(board.::(Mark(x,y,player))) }
    boards
  }


  def computeAnyGame(player: Player, moves: Int): List[Game]  = {

    var boards: List[Board] = List()
    var board: List[Mark] = List()
    var playerTurn = player
    var totalGames: List[Game] = List()

    var allowedMarkMap: Map[Player, List[(Position, Boolean)]] = initializeAllowedMarksMap()

    def placeMarks(keepMarking: Boolean): List[Game] = {

      keepMarking match {
        case false => Nil
        case true => {
          for (remainingMoves <- moves to 1 by -1) {
            var marked = false
            for (x <- 0 to 2; y <- 0 to 2; if canPlayerMark((x, y), allowedMarkMap(playerTurn)); if (!marked)) {
              board = Mark(x, y, playerTurn) :: board
              boards = board :: boards
              marked = true
              allowedMarkMap = temporaryOccupiePosition((x, y), allowedMarkMap)
              if (remainingMoves == 2) {
                totalGames =  considerAllAvailableMoves(playerTurn.other, allowedMarkMap(player), boards) ::: totalGames
                //println("[BEFORE CLEANING "+ allowedMarkMap(playerTurn)+ " " +playerTurn)
                allowedMarkMap = cleanTemporaryOccupiedPositions(allowedMarkMap)
                //println("[AFTER CLEANING "+ allowedMarkMap(playerTurn)+ " " +playerTurn)
                allowedMarkMap = allowedMarkMap ++ permanentOccupiePositiion((x, y), playerTurn, allowedMarkMap(playerTurn))
                //println("[AFTER DELETING "+ allowedMarkMap(playerTurn)+ " " +playerTurn)
              }
              playerTurn = playerTurn.other
            }
          }
          board = Nil
          boards = Nil
        }
          placeMarks(anyAvailableMark(allowedMarkMap))

      }

    }

    placeMarks(true)

  }


  /** This method will add all the possible last move marks, considering the available marks. After compleating the last move,
   * the new game scenario created will be added to the all games container. */
  def considerAllAvailableMoves(player: Player, positions: List[(Position, Boolean)], untilLastBoard: List[Board]): List[Game] =  {
    var games: List[Game] = List()
    for ( position <- positions; if position._2) {
      val updatedBoard = Mark(position._1._1, position._1._2, player) :: untilLastBoard.head
      val newGame = updatedBoard :: untilLastBoard
      games = newGame :: games
    }
    games foreach (g => {println(""); printBoards(g)})
    games
  }

  def temporaryOccupiePosition(position: Position, allowedPositions: Map[Player, List[(Position, Boolean)]]): Map[Player, List[(Position, Boolean)]] = {
      updatePositionsOf(X, occupiePosition(position, allowedPositions(X))) ++
      updatePositionsOf(O, occupiePosition(position, allowedPositions(O)))
  }

  def cleanTemporaryOccupiedPositions(allowedPositions: Map[Player, List[(Position, Boolean)]]): Map[Player, List[(Position, Boolean)]] = {
      Map(X-> cleanOccuptiedPositionsOf(allowedPositions(X)), O -> cleanOccuptiedPositionsOf(allowedPositions(O)))
  }

  def cleanOccuptiedPositionsOf(positions: List[(Position, Boolean)]):List[(Position, Boolean)] =
    positions.map( position => if(!position._2) { (position._1, true)}else (position))

  def permanentOccupiePositiion(position: Position, player: Player, allowedPositions: List[(Position, Boolean)]): Map[Player, List[(Position, Boolean)]] = {
    println("Position to be deleted :" + position + " " + " player "+ player )
    println(Map(player -> removePosition(position, allowedPositions)))
    Map(player -> removePosition(position, allowedPositions))
  }

  def canPlayerMark(position: Position, allowedMarks: List[(Position, Boolean)]): Boolean =
    allowedMarks.filter(p => (p._1 == position)) match {
      case h::_ => h._2
      case _ => false
    }

  def anyAvailableMark(allowedMarks: Map[Player, List[(Position, Boolean)]]): Boolean = !(allowedMarks(X).isEmpty && allowedMarks(O).isEmpty)

  def occupiePosition(position: Position, positions:  List[(Position, Boolean)]): List[(Position, Boolean)] =
    positions.map({ case (p, _) if(p==position)=> (position, false);
                    case (p,b) => (p,b)  })

  def updatePositionsOf(player: Player, updatedPositions: List[(Position, Boolean)]): Map[Player, List[(Position, Boolean)]] = Map(player -> updatedPositions)

  def removePosition(position: Position, positions: List[(Position, Boolean)]): List[(Position, Boolean)] =
    positions.filter(p => p._1!=position)  //collect({ case p if(p._1 != position) => (p._1, p._2)})

  /** Initialise all possible marks as possibile for both players, specifying the flag to false to every mark */
  def initializeAllowedMarksMap(): Map[Player, List[(Position, Boolean)]] = {
    var marks: List[(Position, Boolean)] = List()
    for ( x<-0 to 2; y<-0 to 2 ) marks = ((x,y), true) :: marks
    val allowedMarks:Map[Player, List[(Position, Boolean)]] = Map(X -> marks, O -> marks.map(m => ((m._1._1, m._1._2), m._2)))
    allowedMarks
  }

  def printBoards(game: Seq[Board]): Unit =
    for (y <- 0 to 2; board <- game.reverse; x <- 0 to 2) {
      print(find(board, x, y) map (_.toString) getOrElse ("."))
      if (x == 2) { print(" "); if (board == game.head) println()}
    }

 /* // Exercise 1: implement find such that..
  // println(find(List(Mark(0,0,X)),0,0)) // Some(X)
  // println(find(List(Mark(0,0,X),Mark(0,1,O),Mark(0,2,X)),0,1)) // Some(O)
  // println(find(List(Mark(0,0,X),Mark(0,1,O),Mark(0,2,X)),1,1)) // None

  // Exercise 2: implement placeAnyMark such that..
  //printBoards(placeAnyMark(List(),X))
  //... ... ..X ... ... .X. ... ... X..
  //... ..X ... ... .X. ... ... X.. ...
  //..X ... ... .X. ... ... X.. ... ...

  printBoards(placeAnyMark(List(Mark(0,0,O)),X))
  //O.. O.. O.X O.. O.. OX. O.. O..
  //... ..X ... ... .X. ... ... X..
  //..X ... ... .X. ... ... X.. ... */

  // Exercise 3 (ADVANCED!): implement computeAnyGame such that..
  computeAnyGame(O, 4) foreach {g => printBoards(g); println()}

  //... X.. X.. X.. XO.
  //... ... O.. O.. O..
  //... ... ... X.. X..
  //              ... computes many such games (they should be 9*8*7*6 ~ 3000).. also, e.g.:
  //
  //... ... .O. XO. XOO
  //... ... ... ... ...
  //... .X. .X. .X. .X.

  // Exercise 4 (VERY ADVANCED!) -- modify the above one so as to stop each game when someone won!!


}